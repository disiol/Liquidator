﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Licvidator.Munu
{
    public class Restart : MonoBehaviour
    {
        // Start is called before the first frame update
        public static string IS_RESTART = "is restart";

        void Start()
        {
        }

        void OnMouseUp()
        {
            PlayerPrefs.SetInt(Restart.IS_RESTART, 1);
            SceneManager.LoadScene("Reclama");
        }

        public static bool IsRestart()
        {
            return PlayerPrefs.GetInt(Restart.IS_RESTART, 0) == 1;
        }
    }
}