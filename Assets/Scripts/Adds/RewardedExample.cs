using System;
using Unity.Services.Core;
using Unity.Services.Mediation;
using UnityEngine;

namespace Adds
{
    /// <summary>
    /// Sample Implementation of Unity Mediation
    /// </summary>
    public class RewardedExample : MonoBehaviour
    {
        [Header("Ad Unit Ids"), Tooltip("Ad Unit Ids for each platform that represent Mediation waterfalls.")]
        private string androidAdUnitId = "Android_Rewarded";

        [Tooltip("Ad Unit Ids for each platform that represent Mediation waterfalls.")]
        private string iosAdUnitId = "iOS_Rewarded";

        [Header("Game Ids"),
         Tooltip("[Optional] Specifies the iOS GameId. Otherwise uses the GameId of the linked project.")]
        public string iosGameId;

        [Tooltip("[Optional] Specifies the Android GameId. Otherwise uses the GameId of the linked project.")]
        public string androidGameId;

        IRewardedAd mRewardedAd;

        public void AdUnitIds(string androidGameId, string iosGameId)
        {
            this.androidGameId = androidGameId;
            this.iosGameId = iosGameId;
        }

        async void Start()
        {
            try
            {
                Debug.Log(" Rewarded Initializing...");
                await UnityServices.InitializeAsync(GetGameId());
                Debug.Log("Rewardede Initialized!");
                InitializationComplete();
            }
            catch (Exception e)
            {
                InitializationFailed(e);
            }
        }

        void OnDestroy()
        {
            mRewardedAd?.Dispose();
        }

        InitializationOptions GetGameId()
        {
            var initializationOptions = new InitializationOptions();

#if UNITY_IOS
            if (!string.IsNullOrEmpty(iosGameId))
            {
                initializationOptions.SetGameId(iosGameId);
            }
#elif UNITY_ANDROID
            if (!string.IsNullOrEmpty(androidGameId))
            {
                initializationOptions.SetGameId(androidGameId);
            }
#endif
            return initializationOptions;
        }


        private async void ShowRewarded()
        {
            if (mRewardedAd?.AdState == AdState.Loaded)
            {
                try
                {
                    RewardedAdShowOptions showOptions = new RewardedAdShowOptions();
                    showOptions.AutoReload = true;
                    await mRewardedAd.ShowAsync(showOptions);
                    Debug.Log("Rewarded Shown!");
                }
                catch (ShowFailedException e)
                {
                    Debug.LogWarning($"Rewarded failed to show: {e.Message}");
                }
            }
        }

        public async void ShowRewardedWithOptions()
        {
            if (mRewardedAd?.AdState == AdState.Loaded)
            {
                try
                {
                    //Here we provide a user id and custom data for server to server validation.
                    RewardedAdShowOptions showOptions = new RewardedAdShowOptions();
                    showOptions.AutoReload = true;
                    S2SRedeemData s2SData;
                    s2SData.UserId = "my cool user id";
                    s2SData.CustomData = "{\"reward\":\"Gems\",\"amount\":20}";
                    showOptions.S2SData = s2SData;

                    await mRewardedAd.ShowAsync(showOptions);
                    Debug.Log("Rewarded Shown!");
                }
                catch (ShowFailedException e)
                {
                    Debug.LogWarning($"Rewarded failed to show: {e.Message}");
                }
            }
        }

        async void LoadAd()
        {
            try
            {
                await mRewardedAd.LoadAsync();
            }
            catch (LoadFailedException)
            {
                // We will handle the failure in the OnFailedLoad callback
            }
        }

        void InitializationComplete()
        {
            // Impression Event
            MediationService.Instance.ImpressionEventPublisher.OnImpression += ImpressionEvent;

            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    mRewardedAd = MediationService.Instance.CreateRewardedAd(this.androidAdUnitId);
                    break;

                case RuntimePlatform.IPhonePlayer:
                    mRewardedAd = MediationService.Instance.CreateRewardedAd(this.iosAdUnitId);
                    break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.LinuxEditor:
                    mRewardedAd = MediationService.Instance.CreateRewardedAd(!string.IsNullOrEmpty(androidAdUnitId)
                        ? androidAdUnitId
                        : iosAdUnitId);
                    break;
                default:
                    Debug.LogWarning("Mediation service is not available for this platform:" + Application.platform);
                    return;
            }

            // Load Events
            mRewardedAd.OnLoaded += AdLoaded;
            mRewardedAd.OnFailedLoad += AdFailedLoad;

            // Show Events
            mRewardedAd.OnUserRewarded += UserRewarded;
            mRewardedAd.OnClosed += AdClosed;

            Debug.Log($"Initialized On Start. Loading Ad...");

            LoadAd();
        }

        void InitializationFailed(Exception error)
        {
            SdkInitializationError initializationError = SdkInitializationError.Unknown;
            if (error is InitializeFailedException initializeFailedException)
            {
                initializationError = initializeFailedException.initializationError;
            }

            Debug.Log($"Initialization Failed: {initializationError}:{error.Message}");
        }

        void UserRewarded(object sender, RewardEventArgs e)
        {
            Debug.Log($"User Rewarded! Type: {e.Type} Amount: {e.Amount}");
        }

        void AdClosed(object sender, EventArgs args)
        {
            Debug.Log("Rewarded Closed! Loading Ad...");
            AdsManedger.StartGame();
        }

        void AdLoaded(object sender, EventArgs e)
        {
            Debug.Log("Ad loaded");
            ShowRewarded();
        }

        void AdFailedLoad(object sender, LoadErrorEventArgs e)
        {
            Debug.Log("Failed to load ad");
            Debug.Log(e.Message);
            AdsManedger.StartGame();
        }

        void ImpressionEvent(object sender, ImpressionEventArgs args)
        {
            var impressionData = args.ImpressionData != null ? JsonUtility.ToJson(args.ImpressionData, true) : "null";
            Debug.Log($"Impression event from ad unit id {args.AdUnitId} : {impressionData}");
        }
    }
}