using System;
using Unity.Services.Core;
using Unity.Services.Mediation;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Adds
{
    /// <summary>
    /// Sample Implementation of Unity Mediation
    /// </summary>
    public class InterstitialExample : MonoBehaviour
    {
        [Header("Ad Unit Ids"), Tooltip("Ad Unit Ids for each platform that represent Mediation waterfalls.")]
        private string androidAdUnitId = "Android_Interstitial";

        [Tooltip("Ad Unit Ids for each platform that represent Mediation waterfalls.")]
        private string iosAdUnitId = "iOS_Interstitial";
        
        [Header("Game Ids"),
         Tooltip("[Optional] Specifies the iOS GameId. Otherwise uses the GameId of the linked project.")]
        public string iosGameId;

        [Tooltip("[Optional] Specifies the Android GameId. Otherwise uses the GameId of the linked project.")]
        public string androidGameId;

        IInterstitialAd mInterstitialAd;
        
        public void AdUnitIds(string androidGameId, string iosGameId)
        {
            this.androidGameId = androidGameId;
            this.iosGameId = iosGameId;
        }


        async void Start()
        {
            try
            {
                Debug.Log("Initializing...");
                await UnityServices.InitializeAsync(GetGameId());
                Debug.Log("Initialized!");
                InitializationComplete();
            }
            catch (Exception e)
            {
                InitializationFailed(e);
            }
        }

        void OnDestroy()
        {
            mInterstitialAd?.Dispose();
        }
        
      
        InitializationOptions GetGameId()
        {
            var initializationOptions = new InitializationOptions();

#if UNITY_IOS
            if (!string.IsNullOrEmpty(iosGameId))
            {
                initializationOptions.SetGameId(iosGameId);
            }
#elif UNITY_ANDROID
            if (!string.IsNullOrEmpty(androidGameId))
            {
                initializationOptions.SetGameId(androidGameId);
            }
#endif
            return initializationOptions;
        }

        private async void ShowInterstitial()
        {
            if (mInterstitialAd?.AdState == AdState.Loaded)
            {
                try
                {
                    InterstitialAdShowOptions showOptions = new InterstitialAdShowOptions();
                    showOptions.AutoReload = true;
                    await mInterstitialAd.ShowAsync(showOptions);
                    Debug.Log("Interstitial Shown!");
                }
                catch (ShowFailedException e)
                {
                    Debug.Log($"Interstitial failed to show : {e.Message}");
                }
            }
        }

        async void LoadAd()
        {
            try
            {
                await mInterstitialAd.LoadAsync();
            }
            catch (LoadFailedException)
            {
                // We will handle the failure in the OnFailedLoad callback
            }
        }

        void InitializationComplete()
        {
            // Impression Event
            MediationService.Instance.ImpressionEventPublisher.OnImpression += ImpressionEvent;

            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    mInterstitialAd = MediationService.Instance.CreateInterstitialAd(this.androidAdUnitId);
                    break;

                case RuntimePlatform.IPhonePlayer:
                    mInterstitialAd = MediationService.Instance.CreateInterstitialAd(this.iosAdUnitId);
                    break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.LinuxEditor:
                    mInterstitialAd =
                        MediationService.Instance.CreateInterstitialAd(!string.IsNullOrEmpty(this.androidAdUnitId)
                            ? this.androidAdUnitId
                            : iosAdUnitId);
                    break;
                default:
                    Debug.LogWarning("Mediation service is not available for this platform:" + Application.platform);
                    return;
            }

            // Load Events
            mInterstitialAd.OnLoaded += AdLoaded;
            mInterstitialAd.OnFailedLoad += AdFailedLoad;

            // Show Events
            mInterstitialAd.OnClosed += AdClosed;

            Debug.Log("Initialized On Start! Loading Ad...");
            LoadAd();
        }

        void InitializationFailed(Exception error)
        {
            SdkInitializationError initializationError = SdkInitializationError.Unknown;
            if (error is InitializeFailedException initializeFailedException)
            {
                initializationError = initializeFailedException.initializationError;
            }

            Debug.Log($"Initialization Failed: {initializationError}:{error.Message}");
        }

        void AdClosed(object sender, EventArgs args)
        {
            Debug.Log("Interstitial Closed! Loading Ad...");
            AdsManedger.StartGame();
        }

        void AdLoaded(object sender, EventArgs e)
        {
            Debug.Log("Ad loaded");
            ShowInterstitial();
        }

        void AdFailedLoad(object sender, LoadErrorEventArgs e)
        {
            Debug.Log("Failed to load ad");
            Debug.Log(e.Message);
            AdsManedger.StartGame();
        }


        void ImpressionEvent(object sender, ImpressionEventArgs args)
        {
            var impressionData = args.ImpressionData != null ? JsonUtility.ToJson(args.ImpressionData, true) : "null";
            Debug.Log($"Impression event from ad unit id {args.AdUnitId} : {impressionData}");
        }
    }
}