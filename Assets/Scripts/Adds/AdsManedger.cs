﻿using System;
using System.Collections;
using Licvidator.Munu;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Adds
{
    public class AdsManedger : MonoBehaviour
    {
        [Header("Ad Unit Ids"), Tooltip("Ad Unit Ids for each platform that represent Mediation waterfalls."),
         SerializeField]
        private string androidGameId;

        [Tooltip("Ad Unit Ids for each platform that represent Mediation waterfalls."), SerializeField]
        private string iosAdGameId;

        private RewardedExample rewardedExample;

        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(CheckInternetConnection((isConnected) =>
            {
                if (!isConnected)
                {
                    StartGame();
                }
                else
                {
                    ShowAds();
                }
            }));
        }

        private void ShowAds()
        {
            bool restart = Restart.IsRestart();

            if (restart)
            {
                ShowInterstitial();
            }
            else if (!restart)
            {
                ShowNonSkippable();
            }
        }

        private void ShowNonSkippable()
        {
            Debug.Log("ShowNonSkippable");


            gameObject.AddComponent<RewardedExample>().AdUnitIds(androidGameId, iosAdGameId);
        }

        private void ShowInterstitial()
        {
            Debug.Log("ShowInterstitialExample");

            gameObject.AddComponent<InterstitialExample>().AdUnitIds(androidGameId, iosAdGameId);
        }

        IEnumerator CheckInternetConnection(Action<bool> action)
        {
            WWW www = new WWW("http://google.com");
            yield return www;
            if (www.error != null)
            {
                action(false);
            }
            else
            {
                action(true);
            }
        }

        public void ReclamaStart()
        {
            Debug.Log("AdsManedger  ReclamaStart = ");

            SceneManager.LoadScene("Reclama");
        }

        public static void StartGame()
        {
            SceneManager.LoadScene("Scenes/likvidator/licvidator");
        }
        // Update is called once per frame
    }
}